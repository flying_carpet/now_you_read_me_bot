# Now your read me

Telegram bot for speech recognition and transcription using [teloxide](https://docs.rs/teloxide) and the [wit.ai API](https://wit.ai)

## Installation

### Build from source

*Prerequisites: a working rust installation (default config will do) and an installation of openssl-dev in your library search path (i.e. Ubuntu package "libssl-dev")*

1. Clone the git repository
2. cd /here/is/the/cloned/repo
3. cargo build --release

## Usage

After building, you can find the ready-to-use binary in the folder */here/is/the/cloned/repo/target/release/now-you-read-me*.

You can copy that binary to any location you like. Then execute it by running \"BOT_TOKEN=\<YOUR TELEGRAM BOT TOKEN\> WIT_TOKEN=\<YOUR WIT CLIENT TOKEN\> (MOTD=\"optional message of the day that gets displayed after each response of the bot\") ./now-you-read-me \<RUNTIME DIRECTORY\>\"

You can choose your runtime directory freely, but it is currently only used for temporary file storage. Therefore it is highly recommended to use a (empty) directory in a tmpfs.

To create one, you could use a bash script that firstly creates the directory and then starts the bot by executing the command explained above.
