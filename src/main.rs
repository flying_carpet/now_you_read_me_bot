mod speech_recognition;

#[macro_use]
extern crate lazy_static;

use std::env;
use serde::Deserialize;
use teloxide::prelude::*;

lazy_static! {
	static ref HTTP_CLIENT: reqwest::Client = reqwest::Client::new();
	static ref ARGS: Vec<String> = env::args().collect::<Vec<String>>();
	static ref TOKEN: String = match env::var("BOT_TOKEN") {
		Ok(res) => res,
		Err(_) => { panic!("Usage:\nBOT_TOKEN=<YOUR BOT TOKEN> WIT_TOKEN=<WIT CLIENT TOKEN> {} <PATH TO RUNTIME DIRECTORY>", ARGS[0]); }
	};
	static ref WIT_TOKEN: String = match env::var("WIT_TOKEN") {
		Ok(res) => res,
		Err(_) => { panic!("Usage:\nBOT_TOKEN=<YOUR BOT TOKEN> WIT_TOKEN=<WIT CLIENT TOKEN> {} <PATH TO RUNTIME DIRECTORY>", ARGS[0]); }
	};
	static ref MOTD: String = match env::var("MOTD") {
		Ok(res) => String::from("\n\n") + &res,
		Err(_) => String::new()
	};
}

#[derive(Debug, Deserialize)]
struct TelegramResponse {
	//ok: bool,
	result: FileInfo,
}

#[derive(Debug, Deserialize)]
struct FileInfo {
	file_path: String,
}

const PREFIX_MESSAGE: &str = "[message]: ";
const PREFIX_VOICE: &str = "[voice]: ";

// Main function
#[tokio::main]
async fn main() {
	pretty_env_logger::init();
	// check user input
	if ARGS.len() != 2 {
		eprintln!("Usage:\nBOT_TOKEN=<YOUR BOT TOKEN> WIT_TOKEN=<WIT CLIENT TOKEN> {} <PATH TO RUNTIME DIRECTORY>", ARGS[0]);
		return;
	}
	
	speech_recognition::init(&ARGS[1], &WIT_TOKEN);
	
	let bot = teloxide::Bot::new(TOKEN.to_string());
	
	teloxide::repl(bot, handle_message).await;
}

async fn handle_message(bot: Bot, message: Message) -> ResponseResult<()> {
	let msg = message.clone();
	
	match msg.voice() {
		Some(voice) => {
			// Message is a voice message, start transcribing
			println!("{}Voice message received!", PREFIX_MESSAGE);
			let processing_result = handle_voice(voice.file.id.clone(), msg.chat.id.0).await;
			let mut response = if processing_result.is_err() {
				"Sorry, I couldn't recognize that :(\nIf this happens repeatedly, please contact my admin!".to_string()
			}
			else {
				processing_result.unwrap()
			};
			response = response + &MOTD;
			bot.send_message(msg.chat.id, response).reply_to_message_id(msg.id).send().await?;
		}
		None => ()
	};
	
	match msg.video_note() {
		Some(video_note) => {
			// Message is a voice message, start transcribing
			println!("{}Video note received!", PREFIX_MESSAGE);
			let processing_result = handle_voice(video_note.file.id.clone(), msg.chat.id.0).await;
			let mut response = if processing_result.is_err() {
				"Sorry, I couldn't recognize that :(\nIf this happens repeatedly, please contact my admin!".to_string()
			}
			else {
				processing_result.unwrap()
			};
			response = response + &MOTD;
			bot.send_message(msg.chat.id, response).reply_to_message_id(msg.id).send().await?;
		}
		None => ()
	};
	
	Ok(())
}

async fn handle_voice(file_id: String, chat_id: i64) -> Result<String, ()> {
	
	// The wrapper's file download function is buggy, so we do it manually until it will be fixed
	// request file details
	let file_info_response = HTTP_CLIENT.get(format!("https://api.telegram.org/bot{}/getFile?file_id={}", TOKEN.as_str(), file_id)).send().await;
	let file_info = match file_info_response {
		Ok(res) => res,
		Err(error) => {
			eprintln!("{}Error: request for file details failed: {error}", PREFIX_VOICE);
			return Err(());
		}
	};
	if file_info.status() != 200 {
		eprintln!("{}Error: Non-200 status code for request for message details: {}\nDetails: {}", PREFIX_VOICE, file_info.status(), file_info.url());
		return Err(());
	}
	
	// parsing the response
	let file_details_json = file_info.json::<TelegramResponse>().await;
	let file_details_response = match file_details_json {
		Ok(res) => res,
		Err(error) => {
			eprintln!("{}Error: request for file details could not be parsed: {error}", PREFIX_VOICE);
			return Err(());
		}
	};
	
	// request file
	let file_response = HTTP_CLIENT.get(format!("https://api.telegram.org/file/bot{}/{}", TOKEN.as_str(), file_details_response.result.file_path)).send().await;
	let file_received = match file_response {
		Ok(res) => res.bytes().await,
		Err(error) => {
			eprintln!("{}Error: request for file failed: {error}", PREFIX_VOICE);
			return Err(());
		}
	};
	let file = match file_received {
		Ok(res) => res,
		Err(error) => {
			eprintln!("{}Error: request for file failed: {error}", PREFIX_VOICE);
			return Err(());
		}
	};
	
	let recognized = speech_recognition::recognize_wit(file, chat_id).await;
	
	if recognized.is_err() { return Err(()); }
	Ok(recognized.unwrap().to_string())
}
