use std::process::Command;
use std::sync::RwLock;
use std::path::PathBuf;
use std::fs::{self, File};
use std::io::Write;
use serde::Deserialize;
use bytes::Bytes;
use async_std::task::sleep;

lazy_static! {
	static ref VOICE_DIR: RwLock<String> = RwLock::new(String::from(""));
	static ref WIT_TOKEN: RwLock<String> = RwLock::new(String::from(""));
	static ref HTTP_CLIENT: reqwest::Client = reqwest::Client::new();
}

#[derive(Debug, Deserialize)]
struct WitSentence {
	is_final: Option<bool>,
	text: String,
}

const PREFIX_INIT: &str = "[init_voice]: ";
const PREFIX_RECOGNIZE: &str = "[recognize]: ";
const TRY_AGAIN_INTERVAL: std::time::Duration = std::time::Duration::from_millis(500);

pub fn init(voice_dir: &str, wit_token: &str) -> bool {
	// set "global" variables
	let mut env_voice_dir = VOICE_DIR.write().unwrap();
	*env_voice_dir = voice_dir.to_string();
	drop(env_voice_dir);
	
	// save wit token
	let mut env_wit_token = WIT_TOKEN.write().unwrap();
	*env_wit_token = wit_token.to_string();
	drop(env_wit_token);
	
	println!("{}init complete", PREFIX_INIT);
	true
}

pub async fn recognize_wit(input: Bytes, chat_id: i64) -> Result<String, Box<dyn std::error::Error>> {
	// this function panics if the directory does not exist or is not writable! Checks need to be made before calling the function!
	
	// get "global" variables
	let voice_dir = VOICE_DIR.read().unwrap().clone();
	// prepare directory for saving the audio and converting it using the runtime system's ffmpeg
	let mut voice_path = PathBuf::from(voice_dir);
	voice_path.push(chat_id.to_string());
	
	// check if path exists and wait for last request to finish if necessary
	for i in 1..121 {
		if i > 120 {
			return Err("queue busy".into());
		}
		if voice_path.is_file() {
			sleep(TRY_AGAIN_INTERVAL).await;
		}
		else { break; }
	}
	
	let mut voice_file = match File::create(&voice_path) {
		Ok(res) => res,
		Err(error) => {
			eprintln!("{}Error: file creation failed: {error}", PREFIX_RECOGNIZE);
			return Err(Box::new(error));
		}
	};
	match voice_file.write_all(input.as_ref()) {
		Ok(()) => {
			match voice_file.flush() {
				Ok(()) => {},
				Err(error) => {
					eprintln!("{}Error: writing to file failed: {error}", PREFIX_RECOGNIZE);
					return Err(Box::new(error));
				}
			}
		},
		Err(error) => {
			eprintln!("{}Error: writing to file failed: {error}", PREFIX_RECOGNIZE);
			return Err(Box::new(error));
		}
	};
	
	let path_string = voice_path.to_string_lossy();
	match Command::new("ffmpeg")
		.arg("-i")
		.arg(path_string.as_ref())
		.arg("-ac")
		.arg("1")
		.arg("-ar")
		.arg("16000")
		.arg(format!("{path_string}.wav"))
		.status()
		{
			Ok(res) => {
				if !res.success() {
					eprintln!("{PREFIX_RECOGNIZE}ffmpeg returned an error!");
					return Err("ffmpeg error".into());
				}
			},
			Err(error) => {
				eprintln!("{PREFIX_RECOGNIZE}failed to execute ffmpeg!");
				return Err(Box::new(error));
			},
		};
	
	fs::remove_file(voice_path.clone()).unwrap();
	
	voice_path.pop();
	voice_path.push(chat_id.to_string()+".wav");
	
	let audio_wav = match fs::read(voice_path.clone()) {
		Ok(res) => res,
		Err(error) => {
			eprintln!("{}Error: reading wav failed: {error}", PREFIX_RECOGNIZE);
			return Err(Box::new(error));
		}
	};
	
	fs::remove_file(voice_path).unwrap();

	let voice_api_response = HTTP_CLIENT.post("https://api.wit.ai/dictation")
		.header("Authorization", format!("Bearer {}", WIT_TOKEN.read().unwrap().clone()))
		.header("Content-Type", "audio/wav")
		.body(audio_wav)
		.send()
		.await;
	
	let voice_transcript = match voice_api_response {
		Ok(res) => res,
		Err(error) => {
			eprintln!("{}Error: wit api request failed: {error}", PREFIX_RECOGNIZE);
			return Err(Box::new(error));
		},
	};
	
	if voice_transcript.status() != 200 {
		eprintln!("{}Error: Non-200 status code for wit api request: {}\nDetails: {}", PREFIX_RECOGNIZE, voice_transcript.status(), voice_transcript.url());
		return Err("wit api returned non-200 status code".into());
	}
	
	let api_text = voice_transcript.text().await.unwrap();
	
	let mut transcription = String::new();
	
	let mut level = 0;
	let mut actual_json = String::new();
	let mut content = false;
	let mut parsing_error = false;
	
	for c in api_text.chars() {
		actual_json = actual_json + &c.to_string();
		if c == '{' {
			level = level + 1;
			content = true;
		}
		else if c == '}' {
			level = level - 1;
		}
		if level == 0 {
			if content == true {
				// now we can finally parse real JSON!!!
				let sentence: Result<WitSentence, serde_json::Error> = serde_json::from_str(&actual_json);
				if sentence.is_err() {
					parsing_error = true;
				}
				else {
					let sentence = sentence.unwrap();
					if !sentence.is_final.is_none() {
						// a final result. Append it to transcription!
						transcription = transcription + &sentence.text + " ";
					}
				}
				content = false;
			}
			actual_json = "".to_string();
		}
	}
	
	if parsing_error == true {
		transcription = transcription + "\nSome sentences might miss due to parsing errors."
	}
	
	Ok(transcription)
}
